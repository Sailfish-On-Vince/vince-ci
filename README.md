# SailfishOS (Hybris 15.1 branch) for Xiaomi Redmi Note 5/5 Plus

* 3.1.0.12   - https://gitlab.com/Sailfish-On-Vince/vince-ci/pipelines/83721268



This repository uses gitlab-ci to build the sailfish images for the Xiaomi Redmi Note 5/5 Plus, base on LineageOS 15.1

![Xiaomi Redmi 5 Plus](https://cdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-5-plus-2.jpg "Xiaomi Redmi 5 Plus")

## Device specifications

Basic   | Spec Sheet
-------:|:----------
CPU     | Octa-core 2.02 GHz ARM Cortex A53
Chipset | Qualcomm Snapdragon 625, MSM8953
GPU     | Adreno 506
ROM     | 32/64GB 
RAM     | 3/4GB
Android | 7.1.2
Battery | 4000 mAh
Display | 1080x2160 pixels, 5.99
Rear Camera  | 12MP, PDAF
Front Camera | 5 MP


# Source code
[https://github.com/Sailfish-On-Vince](https://github.com/Sailfish-On-Vince)
